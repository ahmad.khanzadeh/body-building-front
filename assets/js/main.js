// **********************************************************************
// Following function is responsible for displaying the menu
// *********************************************************************
const navMenu = document.getElementById('nav-menu'),
      navToggle=document.getElementById('nav-toggle'),
      navClose=document.getElementById('nav-close');

// ==menu show (It is valid, in case of acontant existance)
if(navToggle){
    navToggle.addEventListener('click',()=>{navMenu.classList.add('show-menu')})
}
// ==hiding the menu(It is valid, in case of acontant existance)
if(navClose){
    navClose.addEventListener('click',()=>{navMenu.classList.remove('show-menu')})
}

//***** */ ٍEnd for displaying the menu


// ===================================================================
// Remove Menu Mobile
// ===================================================================
    const navLink=document.querySelectorAll('.nav__link')

    const linkAction=()=>{
        const navMenu=document.getElementById('nav-menu')
        // if user click on each item in nav( nav__link), this function will remove the 'show-menu' class
        navMenu.classList.remove('show-menu')
    }
    navLink.forEach(elem =>elem.addEventListener('click',linkAction))

//*********** */  End Remove Menu Mobile


// ========================================================
// change background Header
// ========================================================
const scrollHeader=()=>{
    const header=document.getElementById('header')
    // in case user scrolls more than 50, add this class *****never add a dot to the class!!! It waste a lot of time for me
    this.scrollY >=50? header.classList.add('bg-header') 
                     : header.classList.remove('bg-header')
}
window.addEventListener('scroll',scrollHeader)
// ************ End Change Background header

// ========================================================
// Calculate JS
// ========================================================
const calculateFourm=document.getElementById('calculate-form'),
      calculateCm=document.getElementById('calculate-cm'),
      calculateKg=document.getElementById('calculate-kg'),
      calculateMessage=document.getElementById('calculate-message')

const calculateBmi=(e)=>{
    e.preventDefault()
    // if field have a value
    if(calculateCm.value === ''|| calculateKg.value===''){
        // Change color and show a message 
        calculateMessage.classList.remove('color-green')
        calculateMessage.classList.add('color-red')
        calculateMessage.textContent='Fill in the Height and Weight'
        // and remove message three seconds
        setTimeout(()=>{
            calculateMessage.textContent=''
        }, 3000)
    }else{
        // Calculate BMI and display it :
        const cm=calculateCm.value/100,
              kg=calculateKg.value,
              bmi=Math.round(kg/(cm*cm)) 
        if(bmi <18.5){
            calculateMessage.classList.add('color-green')
            calculateMessage.textContent= `Your BMI is ${bmi} and you are skinny`
        } else if(bmi <25) {
            calculateMessage.classList.add('color-green')
            calculateMessage.textContent=`Your BMI is ${bmi} and you are normal `
        } else {
            calculateMessage.classList.add('color-red')
            calculateMessage.textContent=`Your BMI is ${bmi} and you are overweight `
        }
        // Ok clear the calculation
        calculateCm.value=''
        calculateKg.value=''
        setTimeout(()=>{calculateMessage.textContent=''},4000)

    }
}

calculateFourm.addEventListener('submit',calculateBmi)
// End Calculate services

// ========================================================
// Email section
// ========================================================

// I am using emailjs service---it might be not available later
// emailjs.com ---> https://www.emailjs.com/docs/tutorial/creating-contact-form/

const contactForm=document.getElementById('contact-form'),
      contactMessage=document.getElementById('contact-message'),
      contactUser=document.getElementById('contact-user')

// sendEmail is a function which is fire soon as user click on the contactForm (addEventListener click)

const sendEmail=(e) =>{
    e.preventDefault()
    // if noting is filed in input box: make it red and display a text

    if(contactUser.value==''){
        contactMessage.classList.remove('color-green')
        contactMessage.classList.add('color-red')
        contactMessage.textContent='You must enter your email'
        setTimeout(()=>{
            contactMessage.textContent='';
            contactMessage.classList.remove('color-red')
        },3000)
    }else{
        // send email to me---serviceID-TemplateID-#form id -publickey
        emailjs.sendForm('service_8ii265v','template_guf0dsp','#contact-form','VQaq6RG_bXgUd7BNm').then(()=>{
            // when we recived the promiss, show message with green color
            contactMessage.classList.add('green-color')
            contactMessage.textContent='You registered successfully'
            setTimeout(()=>{
                contactMessage.textContent=''
                contactMessage.classList.remove('color-green')
            },3000)
        },(error)=>{
            alert('Opps! Server is not responding.', error)
        })
        // clear the imput 
        contactUser.value=''
    }
}
contactForm.addEventListener('submit', sendEmail)
// end Email 

// ========================================================
// Scroll section
// ========================================================
const section =document.querySelectorAll('section[id]')

const scrollActive=() =>{
        const scrollY=window.pageYOffset

        section.forEach(current =>{
            const sectionHeight=current.offsetHeight,
                  sectionTop=current.offsetTop -58,
                  sectionId=current.getAttribute('id'),
                  sectionsClass=document.querySelector('.nav__menu a[href*=' + sectionId +']')

                  console.log(sectionsClass)
            if(scrollY > sectionTop && scrollY <= sectionTop + sectionHeight){
                sectionsClass.classList.add('active-link')
                setTimeout(()=>{sectionsClass.classList.remove('active-link')},1500)
            }else{
                // sectionsClass.classList.remove('active-link')
            }
        })
}
window.addEventListener('scroll',scrollActive)
// End scroll section

// ========================================================
// ScrollUp function
// ========================================================
const scrollUp=() =>{
    const scrollUp=document.getElementById('scroll-up');
    this.scrollY >=350 ? scrollUp.classList.add('show-scroll')
                       : scrollUp.classList.remove('show-scroll')
}
window.addEventListener('scroll', scrollUp)
// end scrollUp

// ========================================================
// ScrollUp function
// ========================================================
const sr=ScrollReveal({
    // set delay, direction,duration and startpoint for it 
    origin:'top',
    distance: '60px',
    duration:2500,
    delay:400,
})
// call scrollReveal for different elements( based on their classes)
sr.reveal(`.home__data,.footer__container, .footer__group`)
sr.reveal(`.home__img`,{delay: 750, origin: 'bottom'})
sr.reveal(`.logos__img, .program__card, .pricing__card`,{interval: 100})
sr.reveal(`.choose__img, .calculate__content`,{origin: 'left'})
sr.reveal(`.choose__content, .calculate__img`,{origin: 'left'})